//Vladyslav Berezhnyak (1934443)

import java.util.*;

public class RpsGame 
{
	private int wins;
	private int losses;
	private int ties;
	private Random rand = new Random();
	
	public String playRound(String choice)
	{	
		String comp = "";
		String msg = "";
		int cpu = rand.nextInt(3);
		switch(cpu)
		{
			case 0: comp = "rock"; break;
			case 1: comp = "paper"; break;
			case 2: comp = "scissors"; break;
		}
		
		if(choice.equals(comp))
		{
			ties++;
			msg = "Computer also played " + comp + ". It's a tie!";
		}
		
		else if(choice == "rock" && comp == "scissors")
		{
			wins++;
			msg = "Computer plays scissors and the computer lost.";
		}
		
		else if(choice == "rock" && comp == "paper")
		{
			losses++;
			msg = "Computer plays paper and the computer won";
		}
		
		else if(choice == "paper" && comp == "rock")
		{
			wins++;
			msg = "Computer plays rock and the computer lost.";
		}
		
		else if(choice == "paper" && comp == "scissors")
		{
			losses++;
			msg = "Computer plays scissors and the computer won.";
		}
		
		else if(choice == "scissors" && comp == "paper")
		{
			wins++;
			msg = "Computer plays paper and the computer lost.";
		}
		
		else if(choice == "scissors" && comp == "rock")
		{
			losses++;
			msg = "Computer plays rock and the computer won.";
		}
			
		return msg;
	}
	
	public int getWins()
	{
		return this.wins;
	}
	
	public int getLosses()
	{
		return this.losses;
	}
	
	public int getTies()
	{
		return this.ties;
	}
}
