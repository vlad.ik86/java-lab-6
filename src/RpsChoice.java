//Vladyslav Berezhnyak (1934443)

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent>
{
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String playerChoice;
	private RpsGame game;
	
	public RpsChoice(TextField msg, TextField win, TextField loss, TextField tie, String plyrCh, RpsGame vGame)
	{
		this.message = msg;
		this.wins = win;
		this.losses = loss;
		this.ties = tie;
		this.playerChoice = plyrCh;
		this.game = vGame;
	}

	@Override
	public void handle(ActionEvent e) 
	{
		this.message.setText(this.game.playRound(this.playerChoice));
		this.wins.setText("Wins: " + this.game.getWins());
		this.losses.setText("Losses: " + this.game.getLosses());
		this.ties.setText("Ties: " + this.game.getTies());
	}

}
