
public class TestRps
{

	public static void main(String[] args) 
	{
		RpsGame game = new RpsGame();
		System.out.println(game.playRound("scissors"));
		System.out.println(game.playRound("rock"));
		System.out.println(game.playRound("rock"));
		System.out.println(game.playRound("paper"));
		
		System.out.println("\n" + "Wins: " + game.getWins() + "\n" + "Losses: " + game.getLosses() + "\n" + "Ties: " + game.getTies());
	}

}
