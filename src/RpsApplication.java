//Vladyslav Berezhnyak (1934443)

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application
{
	RpsGame game = new RpsGame();
	
	public void start(Stage stage)
	{
		Group root = new Group(); 
		
		//create components
		VBox overall = new VBox();
		HBox textFields = new HBox();
		HBox buttons = new HBox();
		TextField msg = new TextField("Welcome! Make your choice");
		TextField wins = new TextField("Wins: 0");
		TextField losses = new TextField("Losses: 0");
		TextField ties = new TextField("Ties: 0");
		Button rBtn = new Button("rock");
		Button pBtn = new Button("paper");
		Button sBtn = new Button("scissors");

		//button event listeners
		RpsChoice actionR = new RpsChoice(msg, wins, losses, ties, rBtn.getText(), game);
		rBtn.setOnAction(actionR);
		RpsChoice actionP = new RpsChoice(msg, wins, losses, ties, pBtn.getText(), game);
		pBtn.setOnAction(actionP);
		RpsChoice actionS = new RpsChoice(msg, wins, losses, ties, sBtn.getText(), game);
		sBtn.setOnAction(actionS);
		
		//extra properties
		msg.setPrefWidth(300.0);
		msg.setEditable(false);
		wins.setEditable(false);
		losses.setEditable(false);
		ties.setEditable(false);
		
		//build interface
		buttons.getChildren().addAll(rBtn, pBtn, sBtn);
		textFields.getChildren().addAll(msg, wins, losses, ties);
		overall.getChildren().addAll(textFields, buttons);
		root.getChildren().add(overall);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 800, 600); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock, Paper, Scissors!"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
	public static void main(String[] args)
	{
		Application.launch(args);
	}
}
